# Proxmox VE spice HTML5

This is mostly meant for ChromeOS but should also work on Linux.

## Installation

On your PVE instance (as root):

```
$ git clone https://gitlab.com/mjwhitta/pve_spice_html5
$ cd pve_spice_html5
$ ./installer
```

Then on your client (ChromeOS):

```
$ curl -Ls "https://goo.gl/b5MWR6" | bash
```

**Note: Read the script first (unless you don't care about security)**

or if Linux (or PVE):

```
$ git clone https://gitlab.com/mjwhitta/pve_spice_html5
$ cd pve_spice_html5
$ ./installer --client
```

## Uninstall

```
$ cd pve_spice_html5
$ ./installer --uninstall
```

## How to use

On the PVE instance:

```
$ websockify_spice <vmid>
```

Then on ChromeOS (or Linux, or PVE):

```
$ pvespice --pve <pvehost> --user <user>@<domain> <vmid>
```

Now open the link.
